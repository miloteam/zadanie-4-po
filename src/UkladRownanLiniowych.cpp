#include "../inc/UkladRownanLiniowych.hh"
using std::endl;
using std::cout;

UkladRownanL::UkladRownanL(){
    A = Macierz();
    B = Wektor();
}

UkladRownanL::UkladRownanL(Macierz nowe_A, Wektor nowe_B){
    A = nowe_A;
    B = nowe_B;
}

std::istream& operator >> (std::istream &in, UkladRownanL &UklRown){
    Macierz A;
    Wektor B;
    in>>A;
    A.Transpozycja();
    UklRown.SetA(A);
    in>>B;
    UklRown.SetB(B);
    if(in.fail())
        in.setstate(std::ios::failbit);
    return in;
}

std::ostream& operator << ( std::ostream &in,const UkladRownanL  &UklRown){
    Macierz A=UklRown.GetA();
    Wektor B=UklRown.GetB(); 
    in<<"Macierz wspolczynnikow: "<<endl;
    in<<A; 
    in<<"Wektor wyrazow wolnych: "<<endl;
    in<<B;
    if(in.fail())
        in.setstate(std::ios::failbit);
    return in;
} 

Wektor UkladRownanL::Rozwiaz() const{

    Macierz M=A;
    M.Odwroc();
    return M*B;
}

double UkladRownanL::DlugoscWektoraBledu()const{
    Wektor W=Rozwiaz();
    Wektor wynik=A*W;
    Wektor blad=wynik-B;
    return blad.DlugoscWektora();
}
