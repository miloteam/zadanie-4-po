#include <iostream>
#include "../inc/Wektor.hh"
#include "../inc/Macierz.hh"
#include "../inc/UkladRownanLiniowych.hh"
#include <fstream>


using namespace std;

int main()
{
UkladRownanL  UklRown;
double blad;
ifstream plik_wej;
Wektor wynik;

plik_wej.open("funkcja_liniowa.txt");
plik_wej >> UklRown;
if(cin.fail()){
       cout << "Blad wczytywania danych z pliku" << endl;
       cin.clear();
       cin.ignore(1000, '\n');
} 
cout << endl << "Uklad rownan: " <<endl <<UklRown << endl;

wynik = UklRown.Rozwiaz();
cout << "Wynik obliczen: "<< endl <<wynik << endl;

blad=UklRown.DlugoscWektoraBledu();
cout << "Dlugosc wektora bledu obliczen: " << endl << blad << endl;

cout <<"Koniec programu" <<endl;

plik_wej.close();
}