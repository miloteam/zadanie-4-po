#include "../inc/Wektor.hh"
using std::endl;
using std::cerr;

Wektor::Wektor(){
  for(int i=0;i<ROZMIAR;i++){
    tablica[i] = 0;
  }
}

double &Wektor::operator[] (int indeks) { 
  if (indeks < 0 || indeks >= ROZMIAR) {
    cerr << "Blad: wartosc poza zakresem wektora" << endl;
      exit(1);
  }
  return tablica[indeks];
}

const double &Wektor::operator[] (int indeks) const{
  if (indeks < 0 || indeks >= ROZMIAR) {
      cerr << "Blad: wartosc poza zakresem wektora" << endl;
      exit(1);
  }
  return tablica[indeks];
}

std::istream &operator>>(std::istream &in, Wektor &W){
  for(int i=0;i<ROZMIAR;i++){
    in>>W[i];
    if(in.fail())
      in.setstate(std::ios::failbit);
  }
  return in;
}

std::ostream& operator << (std::ostream &out, const Wektor &W){
  for(int i=0;i<ROZMIAR;i++){
    out<<W[i]<<" ";
    if(out.fail())
      out.setstate(std::ios::failbit);
    } 
  return out;
}

Wektor Wektor::operator + (const Wektor & W) const{
  Wektor wynik;
  for(int ind=0;ind<ROZMIAR;ind++){
    wynik.tablica[ind]=tablica[ind]+W.tablica[ind];
  }
  return wynik;
}

Wektor Wektor::operator - (const Wektor & W) const{
  Wektor wynik;
  for(int ind=0;ind<ROZMIAR;ind++){
    wynik.tablica[ind]=tablica[ind]-W.tablica[ind];
  }
  return wynik;
}

double Wektor::operator * (const Wektor & W) const{
  double wynik;
  double iloczyn;
  for(int ind=0;ind<ROZMIAR;ind++){
    iloczyn=tablica[ind]*W.tablica[ind];
    wynik+=iloczyn;
  }
  return wynik;
}

Wektor  Wektor::operator * (double Liczba)const{
  Wektor W;
  for(int ind=0;ind<ROZMIAR;ind++){
    W.tablica[ind]=tablica[ind]*Liczba;
  }
  return W;
}

bool Wektor::operator == (const Wektor &W) const{
  for(int i=0;i<ROZMIAR;i++){
    if(W.tablica[i]-tablica[i] > DOKLADNOSC) return false;
  }
  return true;
}

bool Wektor::operator != (const Wektor &W) const{
  for(int i=0;i<ROZMIAR;i++){
    if(W.tablica[i]-tablica[i] < DOKLADNOSC) return false;
  }
  return true;
}

double Wektor::DlugoscWektora() const{
  Wektor W;
  double wynik;

  for(int i=0;i<ROZMIAR;i++){
    wynik += pow((tablica[i]),2);
  }
  return sqrt(wynik);
}  
   
Wektor operator * (double Liczba, Wektor W){
  for(int i=0;i<ROZMIAR;i++){
    W[i]=Liczba*W[i];
  }
  return W;
}
