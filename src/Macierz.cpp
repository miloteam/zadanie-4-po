#include "../inc/Macierz.hh"
using std::cerr;
using std::endl;
using std::cout;


Macierz::Macierz(){ 
  for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
      tablicaW[i][j]=0; 
    }
  }
}

const Wektor &Macierz::operator[] (int Wiersz) const{
  if (Wiersz < 0 || Wiersz >= ROZMIAR) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  }
  return tablicaW[Wiersz];
}

Wektor &Macierz::operator[] (int Wiersz){
  if (Wiersz < 0 || Wiersz >= ROZMIAR) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  }
  return tablicaW[Wiersz];
}

const double &Macierz::operator() (int Wiersz, int Kolumna) const{
  if ((Wiersz < 0 || Wiersz >= ROZMIAR) && (Kolumna < 0 || Kolumna >= ROZMIAR)) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  }
  return tablicaW[Wiersz][Kolumna];
}

double &Macierz::operator() (int Wiersz, int Kolumna){
  if ((Wiersz < 0 || Wiersz >= ROZMIAR) && (Kolumna < 0 || Kolumna >= ROZMIAR)) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  } 
  return tablicaW[Wiersz][Kolumna];
}
  
Wektor Macierz::ZwrocKolumne(int indeks)const{
  if (indeks < 0 || indeks >= ROZMIAR) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  } 
    
  Wektor Kolumna;
  for(int i=0;i<ROZMIAR;i++){
    Kolumna[i]=tablicaW[i][indeks];
  }
  return Kolumna;
} 


void Macierz::Transpozycja(){

  Macierz kopia = *this;
  for(int i=0; i<ROZMIAR; i++){
    tablicaW[i] = kopia.ZwrocKolumne(i);
  }
}

void Macierz::Odwroc(){
  double Minor[ROZMIAR-1][ROZMIAR-1];
  Macierz kopia = *this;
  double wyznacznik = WyznacznikSarrus();

  if(wyznacznik==0){
    cout<<"Nie istnieje macierz odwrotna"<<endl;
    exit(2);
  }
  double a = 1/wyznacznik;

 int b,c;
  for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
      b=0;
      c=0;
      for(int n=0;n<ROZMIAR;n++){
        for(int z=0;z<ROZMIAR;z++){
          if(n!=i)
          if(z!=j){
            Minor[b][c]=kopia.tablicaW[n][z];
            if(c==1){
              c=0;
              b++;
            } else c++;
          }
        }
      }
      double nowy_wyznacznik=Minor[0][0]*Minor[1][1]-Minor[1][0]*Minor[0][1];
      tablicaW[i][j]=a*pow((-1),(i+j))*nowy_wyznacznik;
    }
  }
  Transpozycja();
}

double Macierz::WyznacznikSarrus(){
  double suma1 = 0;
  double suma2 = 0;
  double iloczyn1 = 1;
  double iloczyn2 = 1;

  for(int i=0; i<ROZMIAR; i++){
    iloczyn1 = 1;
    for(int j=0; j<ROZMIAR; j++){
      iloczyn1 *= tablicaW[j][(i+j)%ROZMIAR];
    }
    suma1 += iloczyn1;
  }

  for(int i=0; i<ROZMIAR; i++){
    iloczyn2 = 1;
    for(int j=0; j<ROZMIAR; j++){
      iloczyn2 *= tablicaW[ROZMIAR-1-j][(i+j)%ROZMIAR];
    }
    suma2 += iloczyn2;
  }

  return suma1 - suma2;
}


Macierz Macierz::operator - (const Macierz & M)const{
  Macierz wynik;
    for(int ind=0;ind<ROZMIAR;ind++){
      wynik.tablicaW[ind]=tablicaW[ind]-M.tablicaW[ind];
    }
  return wynik;
}

Macierz Macierz::operator * (const Macierz & M)const{
  Macierz wynik;
  Macierz iloczyn;
  for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
      Wektor Kolumna=M.ZwrocKolumne(j);
      for(int k=0;k<ROZMIAR;k++){
        iloczyn.tablicaW[i]=tablicaW[i]*Kolumna[i];
        wynik[i][j]+=iloczyn[i][k];
      }
    }
  }
  return wynik;
  } 

Macierz Macierz::operator * (double l)const{
   Macierz M;
   for(int i=0;i<ROZMIAR;i++){
    M.tablicaW[i]=tablicaW[i]*l;
   }
   return M;
 }

Wektor Macierz::operator * (const Wektor &W) const{
  double iloczyn;
  Wektor wynik;  
  for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
      iloczyn=tablicaW[i][j]*W[j];
      wynik[i]+=iloczyn; 
    }
  }
  return wynik;
}

bool Macierz::operator == (const Macierz & M) const{
  for(int i=0;i<ROZMIAR;i++){
    if(M.tablicaW[i]!=tablicaW[i]) return false;
  }
  return true;
 }

bool Macierz::operator != (const Macierz & M) const{
  for(int i=0;i<ROZMIAR;i++){
    if(M.tablicaW[i]==tablicaW[i]) return false;
  }
  return true;
  }

std::istream& operator >> (std::istream &in, Macierz &M){
  for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
      in>>M(i,j);
      if(in.fail())
        in.setstate(std::ios::failbit);
      }
    } 
  return in;
} 

std::ostream& operator << (std::ostream &out, const Macierz &M){
  for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
      out<<M(i,j)<<" ";
      if(out.fail())
        out.setstate(std::ios::failbit);
    }
    out<<endl;
  } 
  return out;
}
