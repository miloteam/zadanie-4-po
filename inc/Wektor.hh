#ifndef WEKTOR_HH
#define WEKTOR_HH

#define DOKLADNOSC 0.01 //dokladnosc porownywania wartosci w wektorze

#include "rozmiar.h"
#include <iostream>
#include <string.h>
#include <math.h>

//Klasa opisująca wektor o dowolnym rozmiarze
class Wektor {

  double tablica[ROZMIAR];

  public:

  Wektor();

  Wektor operator + (const Wektor &W) const;
  Wektor operator - (const Wektor &W) const;
 
  double operator * (const Wektor &W) const;  //Iloczyn skalarny
  Wektor  operator * (double Liczba) const;   //Wektor * Liczba

  bool operator == (const Wektor &W) const;
  bool operator != (const Wektor &W) const;

  double DlugoscWektora() const;   
   
  const double &operator[] (int indeks) const; 
  double &operator[] (int indeks);
  
};


/*      Przeciazenie strumienia wejsciowego (wczytywanie wartosci od uzykownika)

 Parametry:
  in - odwołanie do strumienia
  W - adres zmiennej typu wektor, do ktorej wartosci mają być wpisane

 Warunki wstepne:
  in musi być strumieniem wejściowym
 */
 std::istream& operator >> (std::istream &in, Wektor &W);


/*     Przeciążenie strumienia wyjściowego (wyświetlanie wartości)

 Parametry:
  out - odwołanie do strumienia
  W - wektor, który ma być wyświetlony
 
 Warunki wstępne:
  out musi być strumieniem wyjściowym
   */
std::ostream& operator << (std::ostream &out, const Wektor &W);


Wektor operator * (double Liczba, Wektor W);


#endif
