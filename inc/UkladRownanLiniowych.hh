#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

#include <iostream>
#include "Macierz.hh"
#include "Wektor.hh"
#include "rozmiar.h"

//Klasa opisująca układ równań liniowych zapisanych macierzowo
class UkladRownanL {

  Macierz A;    //Macierz wspolczynnikow

  Wektor B;     //Wektor wyrazów wolnych

  public:

  UkladRownanL();
  UkladRownanL(Macierz nowe_A, Wektor nowe_B);

  Macierz GetA() const {return A;};
  Wektor GetB() const {return B;}; 

  void SetA(const Macierz nowe_A) {A = nowe_A;};
  void SetB(const Wektor nowe_B) {B = nowe_B;};

  Wektor Rozwiaz() const;                  //Rozwiąż układ równań liniowych
  double DlugoscWektoraBledu()const;      //oblicz dlugosc wektora bledu
};


/*      Przeciazenie strumienia wejsciowego (wczytywanie wartosci od uzykownika)

 Parametry:
  in - odwołanie do strumienia
  UklRown - adres zmiennej typu UkladRownanL, do ktorej wartosci mają być wpisane

 Warunki wstepne:
  in musi być strumieniem wejściowym
 */
std::istream& operator >> (std::istream &in, UkladRownanL &UklRown);

/*     Przeciążenie strumienia wyjściowego (wyświetlanie wartości)

 Parametry:
  out - odwołanie do strumienia
  UklRown - układ równań liniowych, który ma być wyświetlony
 
 Warunki wstępne:
  out musi być strumieniem wyjściowym
   */
std::ostream& operator << ( std::ostream &out,const UkladRownanL  &UklRown);


#endif
