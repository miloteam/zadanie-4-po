#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "rozmiar.h"
#include <iostream>
#include "Wektor.hh"
#include<algorithm>

//Klasa opisujaca macierz kwadratową o dowolnym rozmiarze
class Macierz {

  Wektor tablicaW[ROZMIAR]; // Tablica wektorów, będących wierszami macierzy
  
  public:

  Macierz();

  void Transpozycja(); 
  void Odwroc();

  double WyznacznikSarrus();

  Macierz operator + (const Macierz &M)const;
  Macierz operator - (const Macierz &M)const;

  Macierz operator * (const Macierz &M)const;   //Macierz * Macierz
  Macierz operator * (double Liczba)const;      //Macierz * Liczba
  Wektor operator * (const Wektor &W)const;     //Macierz * Wektor

  bool operator == (const Macierz &M) const;
  bool operator != (const Macierz &M) const;

  const Wektor & operator[] (int Wiersz) const;
  Wektor & operator[] (int Wiersz);

  const double & operator() (int Wiersz, int Kolumna) const;
  double & operator() (int Wiersz, int Kolumna);

 Wektor ZwrocKolumne(int indeks)const; 
};

/*      Przeciazenie strumienia wejsciowego (wczytywanie wartosci od uzykownika)

 Parametry:
  in - odwołanie do strumienia
  M - adres zmiennej typu macierz, do ktorej wartosci mają być wpisane

 Warunki wstepne:
  in musi być strumieniem wejściowym
 */
std::istream& operator >> (std::istream &in, Macierz &M);

/*     Przeciążenie strumienia wyjściowego (wyświetlanie wartości)

 Parametry:
  out - odwołanie do strumienia
  M - macierz, który ma być wyświetlony
 
 Warunki wstępne:
  out musi być strumieniem wyjściowym
*/
std::ostream& operator << (std::ostream &out, const Macierz &M);


#endif
